<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ApiController extends Controller
{
    private $privateTextId = [
        73
    ];

    public function PublicTextTitles()
    {
        return $this->TextTitles(null);
    }

    public function TextTitles($accessModifier) {
        $rakosrendezo = \App\Text::join('works', 'work_id', 'works.id')
            ->select(['works.id as works_id', 'reference', 'texts.id as texts_id', 'title', 'translator'])
            ->orderBy('reference')
            ->orderBy('language_id')
            ->orderBy('title')
            ->get();
        $texts = [];
        foreach ($rakosrendezo as $text) {

            // special access texts don't show up in the list
            if ($accessModifier != 'norestrict' && in_array($text->texts_id, $this->privateTextId)) {
                continue;
            }

            $texts[$text->reference][] = [
                'id' => $text->texts_id,
                'title' => $text->title,
                'translator' => $text->translator,
            ];
        }
        return $texts;
    }

    public function TextUnits($text) {
        $istvantelek = \App\Row::select('act', 'scene')
            ->where('text_id', $text)
            ->where('act', '>', 0)
            ->where('scene', '>', 0)
            ->distinct()
            ->orderBy('act')
            ->orderBy('scene')
            ->get();
        $units = [];
        foreach ($istvantelek as $dbRow) {
            if (!isset($units[$dbRow->act])) {
                $units[$dbRow->act] = [$dbRow->scene];
            } else {
                $units[$dbRow->act][] = $dbRow->scene;
            }
        }
        return $units;
    }

    public function Lookup(Request $request, $text) {
        $act = $request->query('act', 1);
        $scene = $request->query('scene', 1);

        $act = !is_numeric($act) ? 1 : $act;
        $scene = !is_numeric($scene) ? 1 : $scene;

        $rows = \App\Row::where('text_id', $text)
            ->where('act', $act)
            ->whereRaw("((scene = 0 AND type = 'head') OR (scene = ?))", [$scene])
            ->orderBy('row')
            ->orderBy('id')
            ->get();

        return $this->MapRowsToComponentData($rows);
    }

    private function MapRowsToComponentData($rows) {
        $data = [];
        $prevTextId = -1;
        $prevTextRowNum = null;
        $prevRowNum = null;
        $prevType = '';
        foreach ($rows as $row) {
            // Felvonás címe
            if ($row->scene == 0 && $row->type == 'head') {
                $data['actTitle'] = $row->text;
            }

            // Szín címe
            elseif ($row->row == 0 && $row->type == 'head') {
                $data['sceneTitle'] = $row->text;
            }

            // Szövegsor folytatása (textchunk)
            elseif (
                $row->row == $prevRowNum
                && in_array($row->type, ['text', 'mins'])
                && in_array($prevType, ['text', 'mins'])
            ) {
                $data['rows'][$row->row][$prevTextId]['chunks'][] = [
                    'id' => $row->id,
                    'type' => $row->type,
                    'text' => $row->text,
                ];
            }

            // Szövegsor folytatása közbevetés után
            elseif (
                $row->row == $prevTextRowNum
                && in_array($row->type, ['text', 'mins'])
                && !in_array($prevType, ['text', 'mins'])
            ) {
                $data['rows'][$row->row][$row->id] = [
                    'num' => '',
                    'chunks' => [[
                        'id' => $row->id,
                        'type' => $row->type,
                        'text' => $row->text,
                        'indentId' => $prevTextId,
                    ]]
                ];
                $prevTextId = $row->id;
            }

            // Egyszerű szövegsor
            elseif (in_array($row->type, ['text', 'mins'])) {
                $data['rows'][$row->row][$row->id] = [
                    'num' => $row->row,
                    'chunks' => [[
                        'id' => $row->id,
                        'type' => $row->type,
                        'text' => $row->text,
                    ]]
                ];
                $prevTextId = $row->id;
                $prevTextRowNum = $row->row;
            }

            // Egyéb sorok (instrukció, speaker)
            elseif (in_array($row->type, ['inst', 'spkr', 'xxxx'])) {
                $data['rows'][$row->row][$row->id] = [
                    'num' => '',
                    'chunks' => [[
                        'id' => $row->id,
                        'type' => $row->type,
                        'text' => $row->text,
                    ]]
                ];
            }

            else {
                throw new \Exception('Lookup: Invalid data in row: ' . var_export($row, true));
            }

            $prevRowNum = $row->row;
            $prevType = $row->type;
        }
        return $data;
    }

    public function Search($query) {
        /**
         * In MYSQL 'LIKE' is case-insensitive. 'LIKE BINARY' is needed for case-sensitive search.
         * https://stackoverflow.com/questions/14007450/how-do-you-force-mysql-like-to-be-case-sensitive
         */
        $results = \App\Row::select(['title', 'text_id', 'act', 'scene', 'row', 'text'])
            ->join('texts', 'rows.text_id', 'texts.id')
            ->where('text', 'LIKE', '%' . $this->escape_like($query) . '%')
            ->where('type', 'text')
            ->orderBy('title')
            ->orderBy('act')
            ->orderBy('scene')
            ->orderBy('row')
            ->get();
        return $results;
    }

    /**
     * Escape special characters for a LIKE query.
     * Source: https://stackoverflow.com/questions/22749182/laravel-escape-like-clause/42028380#42028380
     *
     * @param string $value
     * @param string $char
     *
     * @return string
     */
    private function escape_like(string $value, string $char = '\\'): string
    {
        return str_replace(
            [$char, '%', '_'],
            [$char.$char, $char.'%', $char.'_'],
            $value
        );
    }
}
