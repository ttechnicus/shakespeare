<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Text extends Model
{
    protected $primaryKey = 'id';
    public $timestamps = false;

    /**
     * Get the work that the instance is a text of
     */
    public function work() {
        return $this->belongsTo('App\Work');
    }

    /**
     * Get the language of the text
     */
    public function language() {
        return $this->belongsTo('App\Language');
    }

    /**
     * Get the rows of the text
     */
    public function rows() {
        return $this->hasMany('App\Row');
    }
}
