<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Row extends Model
{
    protected $primaryKey = 'id';
    public $timestamps = false;

    /**
     * Get the text the row belongs to
     */
    public function text() {
        return $this->belongsTo('App\Text');
    }
}
