<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Work extends Model
{
    protected $primaryKey = 'id';
    public $timestamps = false;

    /**
     * Get the texts associated with a work.
     */
    public function texts() {
        return $this->hasMany('App\Text');
    }
}
