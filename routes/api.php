<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/TextTitles', 'ApiController@PublicTextTitles');
Route::get('/TextTitles/{accessModifier}', 'ApiController@TextTitles');
Route::get('/TextUnits/{text}', 'ApiController@TextUnits');
Route::get('/Lookup/{text}', 'ApiController@Lookup');
Route::get('/Search/{query}', 'ApiController@Search');
