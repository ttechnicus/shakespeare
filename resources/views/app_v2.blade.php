<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('app.name') }}</title>

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>

        <!-- Fonts -->

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <style>
            html, body {
                height: 100vh;
                margin: 0;
            }

            .full-height {
                min-height: 100vh;
            }

            .wide-content-width {
                width: 768px;
            }
            .narrow-content-width {
                width: 360px;
            }
            .responsive-content-width {
                width: 360px;
            }
            @media only screen and (min-width: 768px) {
                .responsive-content-width {
                    width: 768px;
                }
            }

            .flex-page-container {
                display: -webkit-flex;
                display: -moz-flex;
                display: -ms-flex;
                display: -o-flex;
                display: flex;
                flex-direction: column;
                align-items: stretch;
                justify-content: flex-start;
            }

            header {
                flex: 0 0;
                align-self: center;
            }

            #headerImage {
                width: 100%;
            }

            main {
                flex: 1;
                border-left: 0.5px solid #e1b8b8;
                border-right: 0.5px solid #e1b8b8;
                background: #fff8e8;
            }
        </style>
    </head>
    <body>
        <div id="app" class="flex-page-container flex-column responsive-content-width full-height mx-auto">
            <header>
                <img src="anamorf.jpg" alt="The Globe" id="headerImage">
            </header>

            <main>
                <reader v-if="menuItem=='reader'" @mainmenu="GoToMenu"{!! " am=\"".(isset($accessModifier) ? $accessModifier : '')."\"" !!}></reader>
                <cinema v-else-if="menuItem=='cinema'" @mainmenu="GoToMenu"></cinema>
                <front-page v-else @mainmenu="GoToMenu"></front-page>
            </main>
        </div>
    </body>
</html>
