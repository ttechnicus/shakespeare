export default
[
{
"title": "All's Well That Ends Well",
"adaptations": [
{
    "title": "All's Well That Ends Well",
    "year": 2013,
    "director": "John Underwood",
    "duration": "122 p",
    "url": "https://www.youtube.com/watch?v=djOkgHXuT2w"
},
{
    "title": "All's Well That Ends Well",
    "year": 2010,
    "director": "UC Davis University Honors Program",
    "duration": "155 p",
    "url": "https://www.youtube.com/watch?v=MhZ6EgOIRBU"
},
]
},
{
"title": "A Midsummer Night's Dream",
"adaptations": [
{
    "title": "A Midsummer Night's Dream",
    "year": 2016,
    "director": "Jeff Casazza",
    "duration": "127 p",
    "url": "https://www.youtube.com/watch?v=7SCA5hIwkbg"
},
{
    "title": "Globe Theatre: A Midsummer Night's Dream",
    "year": 2014,
    "director": "Dale Neill",
    "duration": "132 p",
    "url": "https://www.youtube.com/watch?v=7qti_DFVp1I"
},
{
    "title": "A Midsummer Night's Dream",
    "year": 2018,
    "director": "Casey Wilder Mott",
    "duration": "105 p",
    "url": "https://www.youtube.com/watch?v=rvwtHpBuvDk"
},
{
    "title": "A Midsummer Night's Dream",
    "year": 2017,
    "director": "CRHS ",
    "duration": "106 p",
    "url": "https://www.youtube.com/watch?v=8fIU1tPtIBw"
},
{
    "title": "A Midsummer Night's Dream",
    "year": 2018,
    "director": "Jennifer Feather Youngblood",
    "duration": "110 p",
    "url": "https://www.youtube.com/watch?v=66hrL-5ePm0"
},
{
    "title": "A Midsummer Night's Dream",
    "year": 2013,
    "director": "Christina Keefe",
    "duration": "97 p",
    "url": "https://www.youtube.com/watch?v=0P-bJjrVOtI"
},
{
    "title": "A Midsummer Night's Dream",
    "year": 2014,
    "director": "John Underwood",
    "duration": "115 p",
    "url": "https://www.youtube.com/watch?v=ftqoy2JPU2I"
},
{
    "title": "A Midsummer Night's Dream",
    "year": 1968,
    "director": "Sir Peter Hall",
    "duration": "118 p",
    "url": "https://www.youtube.com/watch?v=4RD-7aRcxmA"
},
{
    "title": "A Midsummer Night's Dream",
    "year": 1695,
    "director": "Joan Kemp-Welch",
    "duration": "109 p",
    "url": "https://www.youtube.com/watch?v=Rx2lQ-iMqMA"
},
{
    "title": "A Midsummer Night's Dream",
    "year": 2018,
    "director": "University of Iowa Department of Theatre Arts",
    "duration": "127 p",
    "url": "https://www.youtube.com/watch?v=sPNtuxDi_CI"
},
{
    "title": "A Midsummer Night's Dream",
    "year": 1960,
    "director": "Peter Larlham",
    "duration": "87 p",
    "url": "https://www.youtube.com/watch?v=bzHu2LUWas0"
},
{
    "title": "A Midsummer Night's Dream",
    "year": 2015,
    "director": "Renaissance Now Theatre and Film",
    "duration": "129 p",
    "url": "https://www.youtube.com/watch?v=ChiGiDXif4g"
},
{
    "title": "A Midsummer Night's Dream",
    "year": 2014,
    "director": "W. Steven Lecky",
    "duration": "132 p",
    "url": "https://www.youtube.com/watch?v=tuMtwA_v8E8"
},
{
    "title": "A Midsummer Night's Dream",
    "year": 2018,
    "director": "Niagara County Community College",
    "duration": "111 p",
    "url": "https://www.youtube.com/watch?v=l1yqnDibn2E"
},
{
    "title": "Szentivánéji álom",
    "year": 2004,
    "director": "Szergej Maszlobojscsikov",
    "duration": "137 p",
    "url": "https://www.youtube.com/watch?v=gnJ3s8zSKd4"
},
{
    "title": "Szentivánéji álom",
    "year": 2008,
    "director": "Solymosi Zsolt",
    "duration": "129 p",
    "url": "https://www.youtube.com/watch?v=K1Ji4tRoVlo"
},
{
    "title": "Szentivánéji álom",
    "year": 1991,
    "director": "Lengyel György",
    "duration": "128 p",
    "url": "https://www.youtube.com/watch?v=anSex9_tVR4"
},
{
    "title": "Szentivánéji álom",
    "year": 2013,
    "director": "Zborovszky Andrea, Galgóczy Judit",
    "duration": "87 p",
    "url": "https://www.youtube.com/watch?v=sJzjcXIDfX8"
},
{
    "title": "Szentivánéji álom (Eötvös gimnázium, Tata)",
    "year": 2002,
    "director": "Naszák István",
    "duration": "73 p",
    "url": "https://www.youtube.com/watch?v=kBrDFUtNWls"
},
{
    "title": "Szentivánéji álom (Szent Benedek Iskolaközpont)",
    "year": 2014,
    "director": "Halász Dániel",
    "duration": "70 p",
    "url": "https://www.youtube.com/watch?v=rC48yXprMss"
},
]
},
{
"title": "Antony and Cleopatra",
"adaptations": [
{
    "title": "Antony and Cleopatra ",
    "year": 2018,
    "director": "George M. Roesler",
    "duration": "107 p",
    "url": "https://www.youtube.com/watch?v=CT0sy42RHA8"
},
{
    "title": "Antony and Cleopatra ",
    "year": 2017,
    "director": "Ryan Leverone",
    "duration": "116 p",
    "url": "https://www.youtube.com/watch?v=pLQtOPfvyGo"
},
]
},
{
"title": "As You Like It",
"adaptations": [
{
    "title": "As You Like It",
    "year": 1936,
    "director": "Paul Czinner",
    "duration": "97 p",
    "url": "https://www.youtube.com/watch?v=wFChichBoPI"
},
{
    "title": "As You Like It",
    "year": 2014,
    "director": "George M. Roesler",
    "duration": "98 p",
    "url": "https://www.youtube.com/watch?v=AG51Mtggx-o"
},
{
    "title": "As You Like It",
    "year": 2013,
    "director": "at Wolfe Park in St. Louis Park",
    "duration": "137 p",
    "url": "https://www.youtube.com/watch?v=dDVnVpgzG5U"
},
{
    "title": "As You Like It",
    "year": 2010,
    "director": "Jeffrey Green",
    "duration": "143 p",
    "url": "https://www.youtube.com/watch?v=uvpKl2DA71Q"
},
{
    "title": "As You Like It",
    "year": 2014,
    "director": "Andrew Clarendon",
    "duration": "152 p",
    "url": "https://www.youtube.com/watch?v=vaoeDOI6_n0"
},
{
    "title": "As You Like It",
    "year": 2015,
    "director": "John Underwood",
    "duration": "116 p",
    "url": "https://www.youtube.com/watch?v=ZTSaCh02s8U"
},
{
    "title": "As You Like It",
    "year": 2019,
    "director": "Christina Keefe",
    "duration": "90 p",
    "url": "https://www.youtube.com/watch?v=Y1frdtY2OCs"
},
{
    "title": "As You Like It",
    "year": 2014,
    "director": "Arjun Pathni",
    "duration": "112 p",
    "url": "https://www.youtube.com/watch?v=8leML7NSlKk"
},
{
    "title": "As You Like It",
    "year": 2017,
    "director": "Stephan \"Cash\" Henry ",
    "duration": "111 p",
    "url": "https://www.youtube.com/watch?v=DqGP_GM6_HI"
},
{
    "title": "No Pants Shakespeare - As You Like It (Zoom)",
    "year": 2020,
    "director": "John \"Ray\" Proctor",
    "duration": "106 p",
    "url": "https://www.youtube.com/watch?v=fM1juzhbGQw"
},
{
    "title": "As You Like It",
    "year": 2018,
    "director": "Patrick Midgley",
    "duration": "70 p",
    "url": "https://www.youtube.com/watch?v=8DupuOrWqs4"
},
{
    "title": "As You Like It",
    "year": 2016,
    "director": "Curt Foxworth",
    "duration": "116 p",
    "url": "https://www.youtube.com/watch?v=FsY4poGXl7o"
},
{
    "title": "As You Like It",
    "year": 2017,
    "director": "Michael Chase",
    "duration": "84 p",
    "url": "https://www.youtube.com/watch?v=8dL7f55N4Lw"
},
{
    "title": "As You Like It",
    "year": 2016,
    "director": "NCC Drama",
    "duration": "113 p",
    "url": "https://www.youtube.com/watch?v=x0F428Fk8PA"
},
{
    "title": "As You Like It",
    "year": 2017,
    "director": "Teater Jubach Macan",
    "duration": "169 p",
    "url": "https://www.youtube.com/watch?v=aabfjPt6Yls"
},
{
    "title": "As You Like It",
    "year": 2016,
    "director": "Marin Shakespeare Company",
    "duration": "124 p",
    "url": "https://www.youtube.com/watch?v=tUpUyo251gM"
},
{
    "title": "Ahogy tetszik",
    "year": 1995,
    "director": "Márton István, Valló Péter",
    "duration": "154 p",
    "url": "https://www.youtube.com/watch?v=hadM4aggyRk"
},
{
    "title": "Ahogy tetszik 1. rész",
    "year": 1983,
    "director": "Székely Gábor",
    "duration": "63 p",
    "url": "https://www.youtube.com/watch?v=XUSTOD49J_I"
},
{
    "title": "Ahogy tetszik 2. rész",
    "year": 1983,
    "director": "Székely Gábor",
    "duration": "87 p",
    "url": "https://www.youtube.com/watch?v=WeipaoCWmsg"
},
{
    "title": "Ahogy tetszik ",
    "year": 2018,
    "director": "Kiss Dávid ",
    "duration": "65 p",
    "url": "https://www.youtube.com/watch?v=UJa4SzWXF1Y"
},
{
    "title": "Ahogy tetszik ",
    "year": 2010,
    "director": "Rudolf Péter",
    "duration": "161 p",
    "url": "https://www.youtube.com/watch?v=MRW6KmgkN_w"
},
]
},
{
"title": "Coriolanus",
"adaptations": [
{
    "title": "Coriolanus",
    "year": 2016,
    "director": "Jessica Gelter",
    "duration": "143 p",
    "url": "https://www.youtube.com/watch?v=jg8H97gwio4"
},
{
    "title": "Coriolanus: Hero without a Country",
    "year": 1963,
    "director": "Giorgio Ferroni",
    "duration": "93 p",
    "url": "https://www.youtube.com/watch?v=nOH9cbJSIZs"
},
{
    "title": "Coriolanus",
    "year": 2019,
    "director": "Jason Purdie, Alisha Kirkland",
    "duration": "112 p",
    "url": "https://www.youtube.com/watch?v=yW7RjjzgzxY"
},
{
    "title": "Coriolanus Part 1.",
    "year": 2011,
    "director": "Ralph Fiennes",
    "duration": "56 p",
    "url": "ttps://videa.hu/videok/film-animacio/coriolanus-2011.cd1-drama-thriller-feliratos-wzn7Mp70NqbWxnWg"
},
{
    "title": "Coriolanus Part 2.",
    "year": 2011,
    "director": "Ralph Fiennes",
    "duration": "66 p",
    "url": "https://videa.hu/videok/film-animacio/coriolanus-2011.cd2.avi-drama-thriller-feliratos-XfM95Kq22pvQnmbh"
},
]
},
{
"title": "Cymbeline",
"adaptations": [
{
    "title": "Cymbeline",
    "year": 2016,
    "director": "John Underwood",
    "duration": "116 p",
    "url": "https://www.youtube.com/watch?v=eGcS6C9_kkY"
},
{
    "title": "Cymbeline",
    "year": 2006,
    "director": "John Malloy",
    "duration": "141 p",
    "url": "https://www.youtube.com/watch?v=-0wObnnANiU"
},
{
    "title": "Cymbeline",
    "year": 2020,
    "director": "Shakespeare Happy Hours",
    "duration": "109 p",
    "url": "https://www.youtube.com/watch?v=W8wRxHcfzEU"
},
]
},
{
"title": "Hamlet",
"adaptations": [
{
    "title": "Hamlet",
    "year": 1996,
    "director": "Kenneth Branagh",
    "duration": "242 p",
    "url": "https://videa.hu/videok/kreativ/hamlet-jyyiqTS8aP6f4XgS"
},
{
    "title": "Hamlet",
    "year": 1990,
    "director": "Franco Zeffirelli",
    "duration": "134 p",
    "url": "https://videa.hu/videok/film-animacio/hamlet-j9N3ZwAZUtdNv57W"
},
{
    "title": "Hamlet",
    "year": 1948,
    "director": "Laurence Olivier",
    "duration": "155 p",
    "url": "https://www.youtube.com/watch?v=wNP_2Omaciw"
},
{
    "title": "Hamlet",
    "year": 2017,
    "director": "Dick Douglass",
    "duration": "76 p",
    "url": "https://www.youtube.com/watch?v=r35_w7AlHyI"
},
{
    "title": "Hamlet",
    "year": 2002,
    "director": "Peter Brook",
    "duration": "132 p",
    "url": "https://www.youtube.com/watch?v=XSfRZmNPHoE"
},
{
    "title": "BLC Theatre presents Hamlet by William Shakespeare",
    "year": 2013,
    "director": "Peter Bloedel",
    "duration": "164 p",
    "url": "https://www.youtube.com/watch?v=zz6GL6AFphU"
},
{
    "title": "JP Stevens Theatre Company Presents Hamlet",
    "year": 2017,
    "director": "Darlene Rich",
    "duration": "156 p",
    "url": "https://www.youtube.com/watch?v=_0iBJ1k6PF8"
},
{
    "title": "Hamlet at Elsinore",
    "year": 1964,
    "director": "Philip Saville",
    "duration": "170 p",
    "url": "https://www.youtube.com/watch?v=l3w_cohUO8E"
},
{
    "title": "Hamlet",
    "year": 1954,
    "director": "Kishore Sahu",
    "duration": "128 p",
    "url": "https://www.youtube.com/watch?v=MU8I2jacvEA"
},
{
    "title": "Hamlet",
    "year": 1984,
    "director": "Esztergályos Károly",
    "duration": "143 p",
    "url": "https://videa.hu/videok/kreativ/hamlet-gj2IHJxhCme0lA8w"
},
{
    "title": "Hamlet",
    "year": 1981,
    "director": "Bódy Gábor",
    "duration": "172 p",
    "url": "https://www.youtube.com/watch?v=kVAXOkfIveM"
},
{
    "title": "Hamlet (Madách Színház)",
    "year": 1963,
    "director": "Vámos László",
    "duration": "215 p",
    "url": "https://www.youtube.com/watch?v=PgXEQHdQd_4"
},
]
},
{
"title": "Henry IV",
"adaptations": [
{
    "title": "Henry IV Part 1.",
    "year": 2018,
    "director": "Courageous Stage and the Town Hall Theater",
    "duration": "71 p",
    "url": "https://www.youtube.com/watch?v=gMQzWdL6zIg"
},
{
    "title": "Henry IV Part 1.",
    "year": 2014,
    "director": null,
    "duration": "113 p",
    "url": "https://www.youtube.com/watch?v=JqUu0fnSHbY"
},
{
    "title": "Henry IV Part 1.",
    "year": 2014,
    "director": "Sevenoaks School",
    "duration": "127 p",
    "url": "https://www.youtube.com/watch?v=68lcORv8zvg"
},
{
    "title": "Henry IV",
    "year": 2017,
    "director": "Sven Delarivière",
    "duration": "163 p",
    "url": "https://www.youtube.com/watch?v=3hItvO_Tpb4"
},
{
    "title": "Henry IV Part 1.",
    "year": 1990,
    "director": "English Shakespeare Company",
    "duration": "170 p",
    "url": "https://www.youtube.com/watch?v=nZ8dAvTGGyw"
},
{
    "title": "Henry IV Part 1.",
    "year": 2017,
    "director": "Jeff Altier",
    "duration": "90 p",
    "url": "https://www.youtube.com/watch?v=2TGvwYosZ_0"
},
{
    "title": "Henry IV Part 2.",
    "year": 1990,
    "director": "English Shakespeare Company",
    "duration": "161 p",
    "url": "https://www.youtube.com/watch?v=sZi2aR8iu1M"
},
{
    "title": "Henry IV Part 2.",
    "year": 2014,
    "director": "Veritas Preparatory Academy",
    "duration": "91 p",
    "url": "https://www.youtube.com/watch?v=qDWittNGs90"
},
{
    "title": "Henry IV Part 2.",
    "year": 2015,
    "director": "Memory Garden",
    "duration": "136 p",
    "url": "https://www.youtube.com/watch?v=gKEt7Rdp190"
},
]
},
{
"title": "Henry VIII",
"adaptations": [
{
    "title": "Henry VIII",
    "year": 2003,
    "director": "Pete Travis",
    "duration": "191 p",
    "url": "https://www.youtube.com/watch?v=_zs6ZBh_U5Y"
},
]
},
{
"title": "Henry VI",
"adaptations": [
{
    "title": "Henry VI Part 1.",
    "year": 1990,
    "director": "Michael Bogdanov",
    "duration": "148 p",
    "url": "https://www.youtube.com/watch?v=EgyXRYclP5o"
},
{
    "title": "Henry VI Part 2.",
    "year": 1990,
    "director": "Michael Bogdanov",
    "duration": "161 p",
    "url": "https://www.youtube.com/watch?v=sZi2aR8iu1M&t=249s"
},
]
},
{
"title": "Henry V",
"adaptations": [
{
    "title": "Henry V",
    "year": 2020,
    "director": "Hal Chambers",
    "duration": "182 p",
    "url": "https://www.youtube.com/watch?v=ynSpVRu_8kE"
},
{
    "title": "Henry V",
    "year": 1990,
    "director": "Michael Bogdanov",
    "duration": "178 p",
    "url": "https://www.youtube.com/watch?v=a3rCRqJP8oY"
},
]
},
{
"title": "Julius Caesar",
"adaptations": [
{
    "title": "Julius Caesar",
    "year": 1969,
    "director": "Alan Bridges",
    "duration": "120 p",
    "url": "https://www.youtube.com/watch?v=JInTNKLaEI4"
},
{
    "title": "Julius Caesar",
    "year": 1970,
    "director": " Stuart Burge",
    "duration": "116 p",
    "url": "https://www.youtube.com/watch?v=mQQh115qAME"
},
{
    "title": "Julius Caesar, Flint Hills Shakespeare Festival",
    "year": 2016,
    "director": "Andrew Clarendon",
    "duration": "150 p",
    "url": "https://www.youtube.com/watch?v=pPHDxw_lHRE"
},
{
    "title": "Julius Caesar ",
    "year": 2002,
    "director": "Uli Edel",
    "duration": "177 p",
    "url": "https://www.youtube.com/watch?v=4nT6ifKkhrY"
},
{
    "title": "Julius Caesar ",
    "year": 2014,
    "director": "Nicolas Walker",
    "duration": "134 p",
    "url": "https://www.youtube.com/watch?v=EesdCfqGE8s"
},
{
    "title": "Julius Caesar",
    "year": 2014,
    "director": "James Loehlin",
    "duration": "129 p",
    "url": "https://www.youtube.com/watch?v=WwJgbyWHNAM"
},
]
},
{
"title": "King John",
"adaptations": [
{
    "title": "King John",
    "year": 2017,
    "director": "Murray Biggs",
    "duration": "85 p",
    "url": "https://www.youtube.com/watch?v=loKFq6FSRx0"
},
{
    "title": "King John",
    "year": 2017,
    "director": "Skagit Valley College Foundation ",
    "duration": "152 p",
    "url": "https://www.youtube.com/watch?v=BmhC7AdEAs8"
},
{
    "title": "King John",
    "year": 2014,
    "director": "Wichita Shakespeare Company",
    "duration": "113 p",
    "url": "https://www.youtube.com/watch?v=qZT8L-_sUwY"
},
]
},
{
"title": "King Lear",
"adaptations": [
{
    "title": "King Lear",
    "year": 2018,
    "director": "Richard Eyre",
    "duration": "115 p",
    "url": "https://www.youtube.com/watch?v=LBonLhmJrQQ"
},
{
    "title": "King Lear - Shakespeare & Company",
    "year": 2011,
    "director": "Jeff Altier",
    "duration": "108 p",
    "url": "https://www.youtube.com/watch?v=MmMkXflriok"
},
{
    "title": "King Lear",
    "year": 2016,
    "director": "Len Falkenstein",
    "duration": "159 p",
    "url": "https://www.youtube.com/watch?v=yLaqoQSAct8"
},
{
    "title": "KOROL LIR / KING LEAR (English subtitles)",
    "year": 1970,
    "director": "Grigori Kozintsev",
    "duration": "131 p",
    "url": "https://www.youtube.com/watch?v=x9-y1Bqs-a8"
},
{
    "title": "Wichita Community Theatre Presents King Lear",
    "year": 2012,
    "director": "Dan Schuster",
    "duration": "161 p",
    "url": "https://www.youtube.com/watch?v=zwoCH52H4ec"
},
{
    "title": "Debreceni Csokonai Színház: Shakespeare: Lear Király",
    "year": 2006,
    "director": "Árkosi Árpád",
    "duration": "187 p",
    "url": "https://www.youtube.com/watch?v=dsvG3G0wPeg"
},
{
    "title": "Lear Király ",
    "year": 1978,
    "director": "Vámos László",
    "duration": "156 p",
    "url": "https://videa.hu/videok/film-animacio/lear-kiraly.avi-CIjzMT5jDvXcIRHw"
},
]
},
{
"title": "Love's Labours Lost",
"adaptations": [
{
    "title": "Love's Labour Lost",
    "year": 1975,
    "director": "Basil Coleman",
    "duration": "119 p",
    "url": "https://www.youtube.com/watch?v=MbbAlpRHKXU"
},
{
    "title": "Love's Labour Lost",
    "year": 2016,
    "director": "Calvin Theatre Company",
    "duration": "151 p",
    "url": "https://www.youtube.com/watch?v=klBibYgGUII"
},
{
    "title": "Love's Labour Lost",
    "year": 2017,
    "director": "Anne Poyner",
    "duration": "125 p",
    "url": "https://www.youtube.com/watch?v=FFNAlcmyYOA"
},
{
    "title": "Love's Labour Lost",
    "year": 2014,
    "director": "Brookline Interactive Club",
    "duration": "110 p",
    "url": "https://www.youtube.com/watch?v=KuIHwK-vOFI"
},
{
    "title": "Love's Labour Lost",
    "year": 2015,
    "director": "Jill Abusch",
    "duration": "91 p",
    "url": "https://www.youtube.com/watch?v=GrcnAObeths"
},
{
    "title": "Love's Labour Lost",
    "year": 2018,
    "director": "Carol Roscoe",
    "duration": "94 p",
    "url": "https://www.youtube.com/watch?v=9Bj1A6xWxEI"
},
{
    "title": "Love's Labour Lost",
    "year": 2015,
    "director": "TASIS School",
    "duration": "112 p",
    "url": "https://www.youtube.com/watch?v=5UcLPYe9S1o"
},
{
    "title": "Love's Labour Lost",
    "year": 2018,
    "director": "Summer Youth Shakespeare Ensemble",
    "duration": "140 p",
    "url": "https://www.youtube.com/watch?v=xMIeAIxf0E0"
},
{
    "title": "Love's Labour Lost",
    "year": 2014,
    "director": "Michael Webber",
    "duration": "116 p",
    "url": "https://www.youtube.com/watch?v=JCnEVomTzuk"
},
{
    "title": "Love's Labour Lost",
    "year": 2018,
    "director": "Shakespeare Kids' Production",
    "duration": "82 p",
    "url": "https://www.youtube.com/watch?v=Bgnph5xwu9A"
},
{
    "title": "Love's Labour Lost",
    "year": 2015,
    "director": "Wendy Almeida",
    "duration": "171 p",
    "url": "https://www.youtube.com/watch?v=1rXC9Qi0aHM"
},
{
    "title": "Love's Labour Lost",
    "year": 2019,
    "director": "Anchor Academy",
    "duration": "75 p",
    "url": "https://www.youtube.com/watch?v=6cT08z8-tCI"
},
{
    "title": "Love's Labour Lost",
    "year": 2013,
    "director": "Winedale",
    "duration": "114 p",
    "url": "https://www.youtube.com/watch?v=Mm6S5cApQG4"
},
{
    "title": "Lóvátett lovagok 1. rész",
    "year": 1980,
    "director": "Szirtes Tamás",
    "duration": "88 p",
    "url": "https://www.youtube.com/watch?v=1fQlq1KNN5w"
},
{
    "title": "Lóvátett lovagok 2. rész",
    "year": 1980,
    "director": "Szirtes Tamás",
    "duration": "77 p",
    "url": "https://www.youtube.com/watch?v=fGHeOhE01K4"
},
]
},
{
"title": "Macbeth",
"adaptations": [
{
    "title": "Macbeth ",
    "year": 2010,
    "director": "Michael Lynch",
    "duration": "138 p",
    "url": "https://www.youtube.com/watch?v=xcD_wlFgeb0"
},
{
    "title": "Macbeth ",
    "year": 2011,
    "director": "Len Falkenstein",
    "duration": "124 p",
    "url": "https://www.youtube.com/watch?v=tggW1_MqmWo"
},
{
    "title": "Macbeth ",
    "year": 1979,
    "director": "Philip Casson",
    "duration": "145 p",
    "url": "https://www.youtube.com/watch?v=7skhaOegpLA"
},
{
    "title": "Globe Theatre: Macbeth",
    "year": 2013,
    "director": "Dale Neill",
    "duration": "118 p",
    "url": "https://www.youtube.com/watch?v=iuCHmNlFfog"
},
{
    "title": "Macbeth ",
    "year": 1981,
    "director": "Arthur Allan Seidelman",
    "duration": "132 p",
    "url": "https://www.youtube.com/watch?v=IKFLVEI1NBA"
},
{
    "title": "Macbeth ",
    "year": 1948,
    "director": "Orson Welles",
    "duration": "80 p",
    "url": "https://www.youtube.com/watch?v=2EaqLy280Oo"
},
{
    "title": "Macbeth ",
    "year": 2020,
    "director": "Ron Pyle",
    "duration": "123 p",
    "url": "https://www.youtube.com/watch?v=ms5wRzOmqG8"
},
{
    "title": "Macbeth ",
    "year": 2013,
    "director": "Dan Hodge ",
    "duration": "87 p",
    "url": "https://www.youtube.com/watch?v=3nwLLNrUHKg"
},
{
    "title": "Macbeth ",
    "year": 2016,
    "director": "Christine Appleby",
    "duration": "106 p",
    "url": "https://www.youtube.com/watch?v=-SHNbhj3lpM"
},
{
    "title": "Macbeth ",
    "year": 2017,
    "director": "Padraig Downey",
    "duration": "95 p",
    "url": "https://www.youtube.com/watch?v=Fz5rlxFM9MQ"
},
{
    "title": "Macbeth ",
    "year": 1997,
    "director": "Jeremy Freeston",
    "duration": "123 p",
    "url": "https://www.youtube.com/watch?v=P2IxA-QErhA"
},
{
    "title": "Macbeth ",
    "year": 2018,
    "director": "Kit Monkman",
    "duration": "120 p",
    "url": "https://www.youtube.com/watch?v=HFgEIzND9ts&has_verified=1"
},
{
    "title": "Macbeth: Santa Catalina School Production",
    "year": 2016,
    "director": "Lara Wheeler Devlin",
    "duration": "100 p",
    "url": "https://www.youtube.com/watch?v=Sbia74md66A"
},
{
    "title": "Macbeth: The Scottish Play in a Galaxy Far, Far Away",
    "year": 2019,
    "director": "Jared Jefferson",
    "duration": "96 p",
    "url": "https://www.youtube.com/watch?v=sT3gjM6N-KQ"
},
{
    "title": "MACBETH (Full Performance) Loomis Chaffee High School",
    "year": 2019,
    "director": "John Howley",
    "duration": "88 p",
    "url": "https://www.youtube.com/watch?v=boshhV-62j4"
},
{
    "title": "Macbeth",
    "year": 2015,
    "director": "Justin Kurzel",
    "duration": "112 p",
    "url": "https://videa.hu/videok/film-animacio/macbeth-2015.-drama-eb0SuLf2ZLZTbp0T"
},
]
},
{
"title": "Measure for Measure",
"adaptations": [
{
    "title": "Measure for Measure",
    "year": 2019,
    "director": "Brown Box Theatre Project",
    "duration": "89 p",
    "url": "https://www.youtube.com/watch?v=sBU29d6GtTI"
},
{
    "title": "Measure for Measure",
    "year": 2015,
    "director": "Eileen Morgan, Rachel Hampton",
    "duration": "159 p",
    "url": "https://www.youtube.com/watch?v=I5peVLtOeug"
},
{
    "title": "Measure for Measure",
    "year": 2019,
    "director": "Emily MacLeod",
    "duration": "58 p",
    "url": "https://www.youtube.com/watch?v=eJj4bknyMv0"
},
{
    "title": "Measure for Measure",
    "year": 2006,
    "director": "Integrated Studies ",
    "duration": "180 p",
    "url": "https://www.youtube.com/watch?v=puLB7xPLGmM"
},
{
    "title": "Measure for Measure Part 1.",
    "year": 2011,
    "director": "A.C.T.",
    "duration": "68 p",
    "url": "https://www.youtube.com/watch?v=akDwiwpmmvE"
},
{
    "title": "Measure for Measure Part 2.",
    "year": 2011,
    "director": "A.C.T.",
    "duration": "57 p",
    "url": "https://www.youtube.com/watch?v=oruIqL79spc"
},
{
    "title": "Szeget szeggel 1. rész ",
    "year": 2014,
    "director": "Tadeusz Bradecki",
    "duration": "59 p",
    "url": "https://www.youtube.com/watch?v=xzcBrbtmZkU"
},
{
    "title": "Szeget szeggel 2. rész ",
    "year": 2014,
    "director": "Tadeusz Bradecki",
    "duration": "92 p",
    "url": "https://www.youtube.com/watch?v=PAdAf_Rmsd8"
},
]
},
{
"title": "Much Ado About Nothing",
"adaptations": [
{
    "title": "Much Ado About Nothing",
    "year": 2011,
    "director": "Josie Rourke",
    "duration": "159 p",
    "url": "https://www.youtube.com/watch?v=Cwy2a6ScZ-c"
},
{
    "title": "Much Ado About Nothing",
    "year": 1993,
    "director": "Kenneth Branagh",
    "duration": "110 p",
    "url": "https://www.youtube.com/watch?v=IUx5p-Os8UY"
},
{
    "title": "Much Ado About Nothing",
    "year": 2011,
    "director": "Dr. Tessa Carr",
    "duration": "141 p",
    "url": "https://www.youtube.com/watch?v=_qEUR-fb298"
},
{
    "title": "Much Ado About Nothing",
    "year": 2018,
    "director": "Jeff & Christina Miller",
    "duration": "120 p",
    "url": "https://www.youtube.com/watch?v=iFLFRAK47w4"
},
{
    "title": "Much Ado About Nothing",
    "year": 2017,
    "director": "Ann Frances Gregg",
    "duration": "119 p",
    "url": "https://www.youtube.com/watch?v=pm9vesIUH6g"
},
{
    "title": "Much Ado About Nothing",
    "year": 2018,
    "director": "Tiffany Ortiz",
    "duration": "110 p",
    "url": "https://www.youtube.com/watch?v=YH_wms4InQs"
},
{
    "title": "Much Ado About Nothing",
    "year": 2019,
    "director": "Aalborg University Shakespeare Company",
    "duration": "141 p",
    "url": "https://www.youtube.com/watch?v=VbFZljVjx9Y"
},
{
    "title": "Much Ado About Nothing (BBC Shakespea-Re-Told)",
    "year": 2005,
    "director": "Brian Percival",
    "duration": "89 p",
    "url": "https://www.youtube.com/watch?v=6UIT8BxEjhc"
},
{
    "title": "Online Shakespeare Reading (Zoom): Much Ado About Nothing ",
    "year": 2020,
    "director": "Nathan Agin",
    "duration": "179 p",
    "url": "https://www.youtube.com/watch?v=XTgkgK3Ss4E"
},
{
    "title": "Much Ado About Nothing",
    "year": 2019,
    "director": "Hyrum Jones",
    "duration": "160 p",
    "url": "https://www.youtube.com/watch?v=4nmiHh3opI4"
},
{
    "title": "Much Ado About Nothing",
    "year": 2011,
    "director": "Aaron Peterson",
    "duration": "120 p",
    "url": "https://www.youtube.com/watch?v=bw6ToCTP6So"
},
{
    "title": "Sok hűhó semmiért ",
    "year": 1993,
    "director": "Kenneth Branagh",
    "duration": "106 p",
    "url": "https://www.youtube.com/watch?v=yEUEqWfUjB8"
},
{
    "title": "Sok hűhó semmiért ",
    "year": 2007,
    "director": "Eszenyi Enikő",
    "duration": "160 p",
    "url": "https://www.youtube.com/watch?v=yZ0K7_7OXco"
},
{
    "title": "Sok hűhó semmiért ",
    "year": 2005,
    "director": "Országúti Társulat",
    "duration": "144 p",
    "url": "https://www.youtube.com/watch?v=ojUGku9TjAc"
},
]
},
{
"title": "Othello",
"adaptations": [
{
    "title": "Shakespeare: Othello, a velencei mór",
    "year": 2003,
    "director": "Tordy Géza",
    "duration": "130 p",
    "url": "https://www.youtube.com/watch?v=xKTXUIP8aIw"
},
{
    "title": "Othello",
    "year": 2013,
    "director": "The Raynham Channel",
    "duration": "126 p",
    "url": "https://www.youtube.com/watch?v=mmw3vp5Boj0"
},
{
    "title": "Tragedy of Othello, The Moor of Venice",
    "year": 1981,
    "director": "Franklin Melton",
    "duration": "180 p",
    "url": "https://www.youtube.com/watch?v=OCdHpKXw_Tg"
},
{
    "title": "William Shakespeare - Othello - U.K modern day adaptation",
    "year": 2001,
    "director": "Geoffrey Sax",
    "duration": "98 p",
    "url": "https://www.youtube.com/watch?v=cIcCE3s_rFc"
},
{
    "title": "Othello (Globe Theatre) Part 1",
    "year": 2008,
    "director": "Wilson Milam",
    "duration": "109 p",
    "url": "https://www.youtube.com/watch?v=LZAElnmqpiw"
},
{
    "title": "Othello (Globe Theatre) Part 2",
    "year": 2008,
    "director": "Wilson Milam",
    "duration": "69 p",
    "url": "https://www.youtube.com/watch?v=D-v2ukM5HUk"
},
{
    "title": "Othello - Madách Színház ",
    "year": 1975,
    "director": "Ádám Ottó",
    "duration": "161 p",
    "url": "https://www.youtube.com/watch?v=nh3-XgiqNSE"
},
{
    "title": "Othello ",
    "year": 2016,
    "director": "John Underwood",
    "duration": "111 p",
    "url": "https://www.youtube.com/watch?v=m0QNINuZ7yE"
},
{
    "title": "Othello - Sea View Playwright's Theatre Company",
    "year": 2015,
    "director": "Sea View Playwright's Theatre Company",
    "duration": "159 p",
    "url": "https://www.youtube.com/watch?v=Q4v4wZDvNG0"
},
{
    "title": "Othello, The Fall of a Warrior",
    "year": 2013,
    "director": "Subramanian Ganesh",
    "duration": "145 p",
    "url": "https://www.youtube.com/watch?v=HeO5d9hpBe0"
},
{
    "title": "Othello ",
    "year": 1952,
    "director": "Orson Welles",
    "duration": "92 p",
    "url": "https://videa.hu/videok/film-animacio/othello-a-velencei-mor-tragediaja.mkv-OhHDvW9p7h8TTrNH"
},
]
},
{
"title": "Richard III",
"adaptations": [
{
    "title": "Richard III",
    "year": 2019,
    "director": "Jean Dobie Giebel",
    "duration": "162 p",
    "url": "https://www.youtube.com/watch?v=-ybjyRxSUBs"
},
{
    "title": "III. Richard 1. rész",
    "year": 2008,
    "director": "Valló Péter",
    "duration": "79 p",
    "url": "https://www.youtube.com/watch?v=ESMKZYPlwFw"
},
{
    "title": "III. Richard 2. rész",
    "year": 2008,
    "director": "Valló Péter",
    "duration": "95 p",
    "url": "https://www.youtube.com/watch?v=F9XSDYeQOiw"
},
{
    "title": "III. Richard",
    "year": 1995,
    "director": "Richard Loncraine",
    "duration": "104 p",
    "url": "https://videa.hu/videok/film-animacio/iii.-richard-104-perc-1995-l8u2NWrsqecQNxBO"
},
{
    "title": "III. Richard",
    "year": 1973,
    "director": "Fehér György",
    "duration": "95 p",
    "url": "https://videa.hu/videok/film-animacio/iii-richard-1973.avi.mp4-verystream.mp4-xFqJT02AGke9Agab"
},
]
},
{
"title": "Richard II",
"adaptations": [
{
    "title": "Richard II",
    "year": 2013,
    "director": "Charles Bouchard",
    "duration": "144 p",
    "url": "https://www.youtube.com/watch?v=e1fkiixQbeM"
},
{
    "title": "Richard II",
    "year": 2019,
    "director": "Keith Scott",
    "duration": "151 p",
    "url": "https://www.youtube.com/watch?v=Ad8LRIyNHlI"
},
{
    "title": "Richard II",
    "year": 1990,
    "director": "Michael Bogdanov",
    "duration": "144 p",
    "url": "https://www.youtube.com/watch?v=d4jmIe2-glc"
},
{
    "title": "Richard II",
    "year": 2017,
    "director": "Susannah Rose Woods",
    "duration": "120 p",
    "url": "https://www.youtube.com/watch?v=tuYFZpolkuw"
},
{
    "title": "Richard II",
    "year": 2017,
    "director": "Shakespeare at San Quentin",
    "duration": "114 p",
    "url": "https://www.youtube.com/watch?v=P-XGC7CVqXE"
},
]
},
{
"title": "Romeo and Juliet",
"adaptations": [
{
    "title": "Romeo and Juliet",
    "year": 2013,
    "director": "Carlo Carlei",
    "duration": "118 p",
    "url": "https://www.youtube.com/watch?v=VBDcDr4XWpY"
},
{
    "title": "ESU Theatre presents William Shakespeare's Romeo and Juliet",
    "year": 2019,
    "director": "Dennis Turney Jr.",
    "duration": "126 p",
    "url": "https://www.youtube.com/watch?v=juSn_IAnwNc"
},
{
    "title": "Romeo and Juliet",
    "year": 1996,
    "director": "Baz Luhrmann",
    "duration": "112 p",
    "url": "https://www.youtube.com/watch?v=WhT7YKeJ2iY"
},
{
    "title": "Romeo and Juliet",
    "year": 2015,
    "director": "Miki Manojlovic",
    "duration": "115 p",
    "url": "https://www.youtube.com/watch?v=qKx6V7Wp0Uo"
},
{
    "title": "The Tragedy of Romeo and Juliet",
    "year": 1982,
    "director": "William Woodman",
    "duration": "169 p",
    "url": "https://www.youtube.com/watch?v=lEOg59Bic3Q"
},
{
    "title": "Romeo and Juliet",
    "year": 2009,
    "director": "John Garrity",
    "duration": "129 p",
    "url": "https://www.youtube.com/watch?v=6idyuKJRK8c"
},
{
    "title": "Romeo and Juliet",
    "year": 2017,
    "director": "Seth Feldman & Brooke Moore",
    "duration": "105 p",
    "url": "https://www.youtube.com/watch?v=rgRY4ok-H2U"
},
{
    "title": "Romeo and Juliet",
    "year": 2013,
    "director": "Gungahlin College production",
    "duration": "110 p",
    "url": "https://www.youtube.com/watch?v=GlEsPBzuHkA"
},
{
    "title": "Romeo and Juliet",
    "year": 2019,
    "director": "Kathryn Donovan",
    "duration": "181 p",
    "url": "https://www.youtube.com/watch?v=RspRfDB-TJk"
},
{
    "title": "Romeo and Juliet",
    "year": 2014,
    "director": "R. Cliff Thompson",
    "duration": "159 p",
    "url": "https://www.youtube.com/watch?v=P3fEK5MQyuM"
},
{
    "title": "Romeo and Juliet",
    "year": 2011,
    "director": "Noam Shmuel ",
    "duration": "140 p",
    "url": "https://www.youtube.com/watch?v=OMc1csie0zQ"
},
{
    "title": "Romeo and Juliet",
    "year": 2008,
    "director": "Emily C. A. Snyder",
    "duration": "153 p",
    "url": "https://www.youtube.com/watch?v=tx22NRHMq-s"
},
{
    "title": "Romeo and Juliet",
    "year": 2013,
    "director": "Vernon Hills High School",
    "duration": "94 p",
    "url": "https://www.youtube.com/watch?v=wFwv66nZ6KQ"
},
{
    "title": "És Rómeó és Júlia",
    "year": 2000,
    "director": "Horgas Ádám",
    "duration": "75 p",
    "url": "https://www.youtube.com/watch?v=BP1cjp19tI4"
},
{
    "title": "Rómeó és Júlia",
    "year": 2016,
    "director": "Szikszai Rémusz",
    "duration": "160 p",
    "url": "https://www.youtube.com/watch?v=KG_ZScMXbi4"
},
{
    "title": "Romeo and Juliet",
    "year": 2019,
    "director": "Meg Sullivan",
    "duration": "141 p",
    "url": "https://www.youtube.com/watch?v=BZYUbhwzUHo"
},
{
    "title": "Middle School Romeo and Juliet Performance",
    "year": 2016,
    "director": "Christian Ely",
    "duration": "78 p",
    "url": "https://www.youtube.com/watch?v=1a9jgF6OYkY"
},
{
    "title": "Romeo and Juliet",
    "year": 2019,
    "director": "Norman Maclean",
    "duration": "123 p",
    "url": "https://www.youtube.com/watch?v=Cs2_mFFV5U4"
},
{
    "title": "Romeo and Juliet",
    "year": 2010,
    "director": "Ronald Reagan HS production",
    "duration": "149 p",
    "url": "https://www.youtube.com/watch?v=e-g4jBaqtWg"
},
{
    "title": "Romeo and Juliet",
    "year": 2017,
    "director": "Trinity School at Greenlawn",
    "duration": "132 p",
    "url": "https://www.youtube.com/watch?v=tr0xh5Ble90"
},
{
    "title": "Romeo and Juliet",
    "year": 2015,
    "director": "Jay Duffer",
    "duration": "146 p",
    "url": "https://www.youtube.com/watch?v=Nb_A1GND3fY"
},
]
},
{
"title": "The Comedy of Errors",
"adaptations": [
{
    "title": "The Comedy of Errors",
    "year": 2019,
    "director": "John Underwood",
    "duration": "111 p",
    "url": "https://www.youtube.com/watch?v=BkaHAPDPBaQ"
},
{
    "title": "The Comedy of Errors",
    "year": 2016,
    "director": "Dave Dahl",
    "duration": "113 p",
    "url": "https://www.youtube.com/watch?v=GzoSLRU_8BI"
},
{
    "title": "The Comedy of Errors",
    "year": 2013,
    "director": "Luke Argles",
    "duration": "76 p",
    "url": "https://www.youtube.com/watch?v=-MyesKS09bk"
},
{
    "title": "The Comedy of Errors",
    "year": 2014,
    "director": "Gileskirk Class of 2014",
    "duration": "67 p",
    "url": "https://www.youtube.com/watch?v=fRj9ak8_eRI"
},
{
    "title": "The Comedy of Errors",
    "year": 2014,
    "director": "Chris McCartie",
    "duration": "112 p",
    "url": "https://www.youtube.com/watch?v=6av0DnYisz4"
},
{
    "title": "The Comedy of Errors",
    "year": 2012,
    "director": "Karla G.-S.",
    "duration": "88 p",
    "url": "https://www.youtube.com/watch?v=rlHCTYCS3y0"
},
{
    "title": "The Comedy of Errors",
    "year": 2018,
    "director": "Yarm School 4th Year",
    "duration": "70 p",
    "url": "https://www.youtube.com/watch?v=lTKav31Mwn0"
},
{
    "title": "The Comedy of Errors",
    "year": 2016,
    "director": "Don't Hate the Players Theatre Company Production",
    "duration": "121 p",
    "url": "https://www.youtube.com/watch?v=AqUMuCweZz0"
},
{
    "title": "The Comedy of Errors",
    "year": 2017,
    "director": "Lisa Shea-Blanchard",
    "duration": "102 p",
    "url": "https://www.youtube.com/watch?v=_7eHOOFhA-M"
},
{
    "title": "The Comedy of Errors",
    "year": 2019,
    "director": "Shakespeare Kids",
    "duration": "55 p",
    "url": "https://www.youtube.com/watch?v=ku1n0jV-5nQ"
},
{
    "title": "The Comedy of Errors",
    "year": 2006,
    "director": "Jason Zembuch",
    "duration": "116 p",
    "url": "https://www.youtube.com/watch?v=4lNGjDRyzZE"
},
{
    "title": "The Comedy of Errors",
    "year": 2010,
    "director": "Emiliy Bosscher, Jeff De Vries",
    "duration": "131 p",
    "url": "https://www.youtube.com/watch?v=MOC-fFOMXLU"
},
{
    "title": "The Comedy of Errors",
    "year": 2015,
    "director": "Concord Players",
    "duration": "76 p",
    "url": "https://www.youtube.com/watch?v=MOC-fFOMXLU"
},
{
    "title": "The Comedy of Errors",
    "year": 2020,
    "director": "Oliver Turner, Rachel Barwise",
    "duration": "89 p",
    "url": "https://www.youtube.com/watch?v=mCdaf9kevhs"
},
{
    "title": "The Comedy of Errors",
    "year": 2018,
    "director": "Jennifer Owen",
    "duration": "114 p",
    "url": "https://www.youtube.com/watch?v=vBFVULDT5ik"
},
{
    "title": "Tévedések vígjátéka, I. felvonás",
    "year": 2017,
    "director": "Bocsárdi László",
    "duration": "72 p",
    "url": "https://www.youtube.com/watch?v=AN_5XA2qipw"
},
{
    "title": "Tévedések vígjátéka, II. felvonás",
    "year": 2017,
    "director": "Bocsárdi László",
    "duration": "70 p",
    "url": "https://www.youtube.com/watch?v=rh4ZXSmgbkk"
},
{
    "title": "Tévedések vígjátéka",
    "year": 1987,
    "director": "Mohácsi János",
    "duration": "114 p",
    "url": "https://www.youtube.com/watch?v=hzG1az0OdRU"
},
{
    "title": "Tévedések vígjátéka 1. rész",
    "year": 2012,
    "director": "Horányné Martikán Erika",
    "duration": "60 p",
    "url": "https://www.youtube.com/watch?v=OONnh3kozqc"
},
{
    "title": "Tévedések vígjátéka 2. rész ",
    "year": 2012,
    "director": "Horányné Martikán Erika",
    "duration": "68 p",
    "url": "https://www.youtube.com/watch?v=BZEHcIi0B0I"
},
]
},
{
"title": "The Mechant of Venice",
"adaptations": [
{
    "title": "The Merchant of Venice",
    "year": 2004,
    "director": "Michael Radford",
    "duration": "131 p",
    "url": "https://www.youtube.com/watch?v=wVaeXnk1tGE"
},
{
    "title": "The Merchant of Venice",
    "year": 1996,
    "director": " Alan Horrox",
    "duration": "81 p",
    "url": "https://www.youtube.com/watch?v=81bNcemYR3U"
},
{
    "title": "The Merchant of Venice",
    "year": 2013,
    "director": "Dori A. Robinson",
    "duration": "133 p",
    "url": "https://www.youtube.com/watch?v=1aoZJbDay5s"
},
{
    "title": "The Merchant of Venice",
    "year": 1973,
    "director": "John Sichel",
    "duration": "127 p",
    "url": "https://www.youtube.com/watch?v=2WGE8lU2pTc"
},
{
    "title": "The Merchant of Venice",
    "year": 2017,
    "director": "The Louche Theatre",
    "duration": "128 p",
    "url": "https://www.youtube.com/watch?v=9m0U00vtSAU"
},
{
    "title": "The Merchant of Venice",
    "year": 2014,
    "director": "TASIS",
    "duration": "131 p",
    "url": "https://www.youtube.com/watch?v=6q-Ug1tJifs"
},
{
    "title": "The Merchant of Venice",
    "year": 2013,
    "director": "performed by inmates at San Quentin State Prison",
    "duration": "115 p",
    "url": "https://www.youtube.com/watch?v=oGUE8PzQczg"
},
]
},
{
"title": "The Merry Wives of Windsor",
"adaptations": [
{
    "title": "The Merry Wives of Windsor",
    "year": 2018,
    "director": "John Underwood",
    "duration": "110 p",
    "url": "https://www.youtube.com/watch?v=MsgTkquJ6CE"
},
{
    "title": "The Merry Wives of Windsor",
    "year": 2019,
    "director": "Jennifer Owen",
    "duration": "112 p",
    "url": "https://www.youtube.com/watch?v=1P4p2wVwplk"
},
{
    "title": "The Merry Wives of Windsor",
    "year": 1965,
    "director": "Georg Tressler",
    "duration": "94 p",
    "url": "https://www.youtube.com/watch?v=RmSl8Ih4OqY"
},
{
    "title": "The Merry Wives of Windsor",
    "year": 2019,
    "director": "John Bukovec",
    "duration": "125 p",
    "url": "https://www.youtube.com/watch?v=eIz1JgqqMUc"
},
{
    "title": "The Merry Wives of Windsor",
    "year": 2019,
    "director": "Asheville School",
    "duration": "129 p",
    "url": "https://www.youtube.com/watch?v=oXYx8clhbWk"
},
]
},
{
"title": "The Taming of the Shrew",
"adaptations": [
{
    "title": "The Taming of the Shrew",
    "year": 2017,
    "director": "John Underwood",
    "duration": "110 p",
    "url": "https://www.youtube.com/watch?v=tccWM8wK2Mo"
},
{
    "title": "The Taming of the Shrew",
    "year": 2016,
    "director": "Richard Herman",
    "duration": "106 p",
    "url": "https://www.youtube.com/watch?v=iKqgXtgcECA"
},
{
    "title": "The Taming of the Shrew",
    "year": 2011,
    "director": "David Edgecombe",
    "duration": "117 p",
    "url": "https://www.youtube.com/watch?v=ris5gVKV99g"
},
{
    "title": "The Taming of the Shrew",
    "year": 1929,
    "director": "Sam Taylor",
    "duration": "66 p",
    "url": "https://www.youtube.com/watch?v=p50ZfYt2RjU"
},
{
    "title": "The Taming of the Shrew",
    "year": 2017,
    "director": "Archelaus Crisanto",
    "duration": "123 p",
    "url": "https://www.youtube.com/watch?v=zb1NqZTXVYo"
},
{
    "title": "The Taming of the Shrew",
    "year": 2004,
    "director": "Integrated Studies Shakespeare",
    "duration": "162 p",
    "url": "https://www.youtube.com/watch?v=_SbAeqjoWD0"
},
{
    "title": "The Show Must Go Online: The Taming Of The Shrew",
    "year": 2020,
    "director": "Rob Myles",
    "duration": "217 p",
    "url": "https://www.youtube.com/watch?v=EAMtHmeHzio"
},
{
    "title": "The Taming of the Shrew",
    "year": 2014,
    "director": "Mike Poole, Sandra Bunday",
    "duration": "139 p",
    "url": "https://www.youtube.com/watch?v=BZFIpdz8isA"
},
{
    "title": "The Taming of the Shrew",
    "year": 2015,
    "director": "Susie Schutt",
    "duration": "107 p",
    "url": "https://www.youtube.com/watch?v=Xtw8qfNqz68"
},
{
    "title": "The Taming of the Shrew",
    "year": 2013,
    "director": "Robin Rightmayer",
    "duration": "120 p",
    "url": "https://www.youtube.com/watch?v=uHxDlFvOxWk"
},
{
    "title": "A makrancos hölgy",
    "year": 2016,
    "director": "Honvéd Kaszinó Városi Diákszínpad",
    "duration": "105 p",
    "url": "https://www.youtube.com/watch?v=CCwnpyNpVWg"
},
]
},
{
"title": "The Tempest",
"adaptations": [
{
    "title": "The Tempest",
    "year": 2010,
    "director": "Julie Taymor",
    "duration": "111 p",
    "url": "https://www.youtube.com/watch?v=IsAlO994niA"
},
{
    "title": "The Tempest Part 1.",
    "year": 2019,
    "director": "RSC Live",
    "duration": "80 p",
    "url": "https://www.youtube.com/watch?v=slvIfbCWcS0"
},
{
    "title": "The Tempest Part 2.",
    "year": 2019,
    "director": "RSC Live",
    "duration": "64 p",
    "url": "https://www.youtube.com/watch?v=Cn79kLx-mmA"
},
{
    "title": "The Tempest",
    "year": 1960,
    "director": "George Schaefer",
    "duration": "77 p",
    "url": "https://www.youtube.com/watch?v=4qheHYn3rLY"
},
{
    "title": "The Tempest",
    "year": 2014,
    "director": "Savage Rose Classical Theatre Company",
    "duration": "125 p",
    "url": "https://www.youtube.com/watch?v=czFoUWwd6mI"
},
{
    "title": "The Tempest",
    "year": 2012,
    "director": "Keith Edie",
    "duration": "95 p",
    "url": "https://www.youtube.com/watch?v=ArQJ9RJJWVA"
},
]
},
{
"title": "The Winter's Tale",
"adaptations": [
{
    "title": "The Winter's Tale",
    "year": 2020,
    "director": "Declan Donnellan",
    "duration": "145 p",
    "url": "https://www.youtube.com/watch?v=3zULvQbgJy8"
},
{
    "title": "The Winter's Tale",
    "year": 2018,
    "director": "John Underwood",
    "duration": "114 p",
    "url": "https://www.youtube.com/watch?v=q9wn5Qo9PYI"
},
{
    "title": "The Winter's Tale",
    "year": 2016,
    "director": "Dr. Pam Monteleone",
    "duration": "143 p",
    "url": "https://www.youtube.com/watch?v=5Ti8urLj02g"
},
{
    "title": "The Winter's Tale",
    "year": 2016,
    "director": "Diana Sung",
    "duration": "96 p",
    "url": "https://www.youtube.com/watch?v=AEHPcQa5jFI"
},
{
    "title": "The Winter's Tale",
    "year": 2013,
    "director": "Jeremy Cole",
    "duration": "155 p",
    "url": "https://www.youtube.com/watch?v=cAIydKlxtik"
},
{
    "title": "The Winter's Tale",
    "year": 2011,
    "director": "The Rude Mechanicals",
    "duration": "120 p",
    "url": "https://www.youtube.com/watch?v=udjeJyMnBXw"
},
{
    "title": "Téli mese",
    "year": 2014,
    "director": "Akiva Goldsman",
    "duration": "118 p",
    "url": "https://videa.hu/videok/film-animacio/teli-mese-modczcOVPh1BYKdn"
},
]
},
{
"title": "Titus Andronicus",
"adaptations": [
{
    "title": "Titus Andronicus, Seoul Shakespeare Company ",
    "year": 2015,
    "director": "Raymond C. Saleedo",
    "duration": "173 p",
    "url": "https://www.youtube.com/watch?v=9LfvLKgx_Ik"
},
{
    "title": "Titus Andronicus",
    "year": 2019,
    "director": "The Lawrenceville School",
    "duration": "83 p",
    "url": "https://www.youtube.com/watch?v=GtDVJCyhAiQ"
},
{
    "title": "Titus Andronicus, Stephen F. Austin School of Theatre",
    "year": 2019,
    "director": "Cleo House Jr.",
    "duration": "131 p",
    "url": "https://www.youtube.com/watch?v=YVedeIqnQ9w"
},
{
    "title": "Titus Andronicus ",
    "year": 2016,
    "director": "Lucette Moran",
    "duration": "128 p",
    "url": "https://www.youtube.com/watch?v=W-uy2_BXCPc"
},
{
    "title": "Titus Andronicus ",
    "year": 2006,
    "director": "Paul Carmichael",
    "duration": "124 p",
    "url": "https://www.youtube.com/watch?v=xIKJtA13v84"
},
{
    "title": "Titus Andronicus",
    "year": 2014,
    "director": "Dan Stevens",
    "duration": "142 p",
    "url": "https://www.youtube.com/watch?v=8j1ynMbP99g"
},
{
    "title": "Titus ",
    "year": 1999,
    "director": " Julie Taymor",
    "duration": "162 p",
    "url": "https://videa.hu/videok/film-animacio/titus-1999-7cXX2XN8ahBrqvaP"
},
]
},
{
"title": "Twelfth Night",
"adaptations": [
{
    "title": "Twelfth Night",
    "year": 1969,
    "director": "John Sichel",
    "duration": "100 p",
    "url": "https://www.youtube.com/watch?v=DuwgQ9Qof88"
},
{
    "title": "Twelfth Night",
    "year": 2019,
    "director": "Flint Hills Shakespeare Festival",
    "duration": "144 p",
    "url": "https://www.youtube.com/watch?v=eC3Nf3-pPsc"
},
{
    "title": "Twelfth Night",
    "year": 1986,
    "director": "Neil Armfield",
    "duration": "118 p",
    "url": "https://www.youtube.com/watch?v=9VuuPhAp-ig"
},
{
    "title": "Twelfth Night",
    "year": 2019,
    "director": "Terry Layman",
    "duration": "146 p",
    "url": "https://www.youtube.com/watch?v=r7Js-IV2rGw"
},
{
    "title": "Twelfth Night",
    "year": 2015,
    "director": "W. Steven Lecky",
    "duration": "136 p",
    "url": "https://www.youtube.com/watch?v=f0PuPlkgV1s"
},
{
    "title": "Twelfth Night",
    "year": 2012,
    "director": "Globe Theatre",
    "duration": "146 p",
    "url": "https://www.youtube.com/watch?v=NE2ZCv-Hp18"
},
{
    "title": "Twelfth Night",
    "year": 2016,
    "director": "Mark Hartfield",
    "duration": "140 p",
    "url": "https://www.youtube.com/watch?v=8tHUSgXYAsY"
},
{
    "title": "Twelfth Night",
    "year": 2013,
    "director": null,
    "duration": "111 p",
    "url": "https://www.youtube.com/watch?v=PEqX75Y0O6o"
},
{
    "title": "Twelfth Night",
    "year": 2015,
    "director": "Anne Frances Gregg",
    "duration": "102 p",
    "url": "https://www.youtube.com/watch?v=X1uBRv5MJbQ"
},
{
    "title": "Twelfth Night",
    "year": 2018,
    "director": "Urban Impact Shakes",
    "duration": "91 p",
    "url": "https://www.youtube.com/watch?v=qAbdype0v7Y"
},
{
    "title": "Twelfth Night",
    "year": 2020,
    "director": "Eden Lee Murray",
    "duration": "86 p",
    "url": "https://www.youtube.com/watch?v=XHTul9mZUkk"
},
{
    "title": "Twelfth Night",
    "year": 2020,
    "director": "Oak Forest High School Drama Group",
    "duration": "143 p",
    "url": "https://www.youtube.com/watch?v=YlTCgDfJTNU"
},
{
    "title": "Vízkereszt vagy amit akartok 1. rész",
    "year": 1992,
    "director": "Lukácsi Huba",
    "duration": "51 p",
    "url": "https://www.youtube.com/watch?v=5NY8GvbgQ7Q"
},
{
    "title": "Vízkereszt vagy amit akartok 2. rész",
    "year": 1992,
    "director": "Lukácsi Huba",
    "duration": "49 p",
    "url": "https://www.youtube.com/watch?v=5FOCHbgqXDk"
},
]
},
{
"title": "Two Gentlemen of Verona",
"adaptations": [
{
    "title": "The Two Gentlemen of Verona",
    "year": 2018,
    "director": "Ilona Pierce",
    "duration": "116 p",
    "url": "https://www.youtube.com/watch?v=BmgpDabaRQ8"
},
{
    "title": "The Two Gentlemen of Verona",
    "year": 2020,
    "director": "The Show Must Go Online",
    "duration": "192 p",
    "url": "https://www.youtube.com/watch?v=KOAHj4ANGKw"
},
{
    "title": "The Two Gentlemen of Verona",
    "year": 2013,
    "director": "Chris McCartie",
    "duration": "114 p",
    "url": "https://www.youtube.com/watch?v=WyX8aVLwWh4"
},
{
    "title": "The Two Gentlemen of Verona",
    "year": 2019,
    "director": "MVC Summer Theatre",
    "duration": "84 p",
    "url": "https://www.youtube.com/watch?v=no4xnKpXsXA"
},
]
},
{
"title": "Two Noble Kinsmen",
"adaptations": [
{
    "title": "Two Noble Kinsmen",
    "year": 2015,
    "director": " Ryan Higgins",
    "duration": "110 p",
    "url": "https://www.youtube.com/watch?v=SMV3IlqQzXE"
},
{
    "title": "Two Noble Kinsmen",
    "year": 2014,
    "director": " Lori Leigh",
    "duration": "140 p",
    "url": "https://www.youtube.com/watch?v=hOKL_iX_wTQ"
},
{
    "title": "Two Noble Kinsmen",
    "year": 1988,
    "director": "Gavin Bantock",
    "duration": "110 p",
    "url": "https://www.youtube.com/watch?v=30w5bVtxXHY"
},
]
},
]
