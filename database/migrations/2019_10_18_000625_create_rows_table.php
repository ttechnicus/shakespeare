<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::enableForeignKeyConstraints();
        Schema::create('rows', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('text_id')->nullable(false);
            $table->unsignedInteger('act')->nullable(false);
            $table->unsignedInteger('scene')->nullable(false);
            $table->unsignedInteger('row')->nullable(false);
            $table->char('type', 4)->nullable(false);
            $table->text('text')->nullable(false);
            $table->foreign('text_id')->references('id')->on('texts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rows');
    }
}
