<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTextsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::enableForeignKeyConstraints();
        Schema::create('texts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('work_id')->nullable(false);
            $table->string('author', 45)->nullable(true);
            $table->string('title', 255)->nullable(false);
            $table->char('language_id', 2)->nullable(false);
            $table->string('translator', 63)->nullable(true);

            $table->foreign('work_id')->references('id')->on('works');
            $table->foreign('language_id')->references('id')->on('languages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('texts');
    }
}
