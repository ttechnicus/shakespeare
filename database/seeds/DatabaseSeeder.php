<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        print "Seeding `languages`...\n";
        DB::table('languages')->insert(['id' => 'en', 'name' => 'angol']);
        DB::table('languages')->insert(['id' => 'hu', 'name' => 'magyar']);

        print "Seeding `works`...\n";
        DB::table('works')->insert(['id' => 1, 'reference' => 'VI. Henrik, I. rész', 'type' => 'királydráma', 'year' => '1591']);
        DB::table('works')->insert(['id' => 2, 'reference' => 'VI. Henrik, II. rész', 'type' => 'királydráma', 'year' => '1591']);
        DB::table('works')->insert(['id' => 3, 'reference' => 'VI. Henrik, III. rész', 'type' => 'királydráma', 'year' => '1591']);
        DB::table('works')->insert(['id' => 4, 'reference' => 'Titus Andronicus', 'type' => 'tragédia', 'year' => '1591']);
        DB::table('works')->insert(['id' => 5, 'reference' => 'Tévedések vígjátéka', 'type' => 'vígjáték', 'year' => '1591']);
        DB::table('works')->insert(['id' => 6, 'reference' => 'III. Richard', 'type' => 'királydráma', 'year' => '1592']);
        DB::table('works')->insert(['id' => 7, 'reference' => 'A makrancos hölgy', 'type' => 'vígjáték', 'year' => '1592']);
        DB::table('works')->insert(['id' => 8, 'reference' => 'János király', 'type' => 'királydráma', 'year' => '1593']);
        DB::table('works')->insert(['id' => 9, 'reference' => 'Lóvátett lovagok', 'type' => 'vígjáték', 'year' => '1593']);
        DB::table('works')->insert(['id' => 10, 'reference' => 'A két veronai nemes', 'type' => 'vígjáték', 'year' => '1593']);
        DB::table('works')->insert(['id' => 11, 'reference' => 'II. Richard', 'type' => 'királydráma', 'year' => '1595']);
        DB::table('works')->insert(['id' => 12, 'reference' => 'Romeo és Júlia', 'type' => 'tragédia', 'year' => '1595']);
        DB::table('works')->insert(['id' => 13, 'reference' => 'Szentivánéji álom', 'type' => 'vígjáték', 'year' => '1595']);
        DB::table('works')->insert(['id' => 14, 'reference' => 'IV. Henrik, I. rész', 'type' => 'királydráma', 'year' => '1596']);
        DB::table('works')->insert(['id' => 15, 'reference' => 'A velencei kalmár', 'type' => 'problémaszínmű', 'year' => '1596']);
        DB::table('works')->insert(['id' => 16, 'reference' => 'IV. Henrik, II. rész', 'type' => 'királydráma', 'year' => '1597']);
        DB::table('works')->insert(['id' => 17, 'reference' => 'A windsori víg nők', 'type' => 'vígjáték', 'year' => '1597']);
        DB::table('works')->insert(['id' => 18, 'reference' => 'Sok hűhó semmiért', 'type' => 'vígjáték', 'year' => '1598']);
        DB::table('works')->insert(['id' => 19, 'reference' => 'Julius Caesar', 'type' => 'tragédia', 'year' => '1599']);
        DB::table('works')->insert(['id' => 20, 'reference' => 'V. Henrik', 'type' => 'királydráma', 'year' => '1599']);
        DB::table('works')->insert(['id' => 21, 'reference' => 'Ahogy tetszik', 'type' => 'vígjáték', 'year' => '1599']);
        DB::table('works')->insert(['id' => 22, 'reference' => 'Vízkereszt vagy amit akartok', 'type' => 'vígjáték', 'year' => '1601']);
        DB::table('works')->insert(['id' => 23, 'reference' => 'Hamlet, dán királyfi', 'type' => 'tragédia', 'year' => '1601']);
        DB::table('works')->insert(['id' => 24, 'reference' => 'Troilus és Cressida', 'type' => 'problémaszínmű', 'year' => '1602']);
        DB::table('works')->insert(['id' => 25, 'reference' => 'Minden jó, ha a vége jó', 'type' => 'problémaszínmű', 'year' => '1603']);
        DB::table('works')->insert(['id' => 26, 'reference' => 'Othello, a velencei mór', 'type' => 'tragédia', 'year' => '1604']);
        DB::table('works')->insert(['id' => 27, 'reference' => 'Szeget szeggel', 'type' => 'problémaszínmű', 'year' => '1604']);
        DB::table('works')->insert(['id' => 28, 'reference' => 'Lear király', 'type' => 'tragédia', 'year' => '1605']);
        DB::table('works')->insert(['id' => 29, 'reference' => 'Macbeth', 'type' => 'tragédia', 'year' => '1606']);
        DB::table('works')->insert(['id' => 30, 'reference' => 'Antonius és Kleopátra', 'type' => 'tragédia', 'year' => '1607']);
        DB::table('works')->insert(['id' => 31, 'reference' => 'Pericles', 'type' => 'regényes színmű', 'year' => '1607']);
        DB::table('works')->insert(['id' => 32, 'reference' => 'Athéni Timon', 'type' => 'tragédia', 'year' => '1608']);
        DB::table('works')->insert(['id' => 33, 'reference' => 'Coriolanus', 'type' => 'tragédia', 'year' => '1608']);
        DB::table('works')->insert(['id' => 34, 'reference' => 'Cymbeline', 'type' => 'regényes színmű', 'year' => '1609']);
        DB::table('works')->insert(['id' => 35, 'reference' => 'A vihar', 'type' => 'regényes színmű', 'year' => '1611']);
        DB::table('works')->insert(['id' => 36, 'reference' => 'Téli rege', 'type' => 'regényes színmű', 'year' => '1611']);
        DB::table('works')->insert(['id' => 37, 'reference' => 'VIII. Henrik', 'type' => 'királydráma', 'year' => '1613']);
    }
}
